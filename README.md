# Desktop for Raspberry PI (2017 -> 202x desktops.)

v1
 

 http://director.downloads.raspberrypi.org/raspbian_lite/images/raspbian_lite-2017-12-01/2017-11-29-raspbian-stretch-lite.zip  

http://director.downloads.raspberrypi.org/raspbian_lite/images/raspbian_lite-2017-12-01/2017-11-29-raspbian-stretch-lite.zip 



![](https://gitlab.com/openbsd98324/piwork/-/raw/main/archive/v1/raspberry-pi-pixel-interface-screen.jpg)

 http://director.downloads.raspberrypi.org/raspbian_lite/images/raspbian_lite-2017-12-01/2017-11-29-raspbian-stretch-lite.zip



# Tested (2023)

https://downloads.raspberrypi.org/raspbian_lite/images/raspbian_lite-2017-12-01/2017-11-29-raspbian-stretch-lite.zip

c4ed01ab67dcb2e209d558334eb33dc76ae58469  2017-11-29-raspbian-stretch-lite.zip

https://downloads.raspberrypi.org/raspbian/images/

https://downloads.raspberrypi.org/raspbian/images/raspbian-2017-12-01/2017-11-29-raspbian-stretch.zip
1.6G


# Partition




````


Disk /dev/mmcblk0 - 15 GB / 14 GiB - CHS 486192 4 16
Analyse cylinder 470495/486191: 96%


check_FAT: Unusual number of reserved sectors 8 (FAT), should be 1.
Warning: number of heads/cylinder mismatches 64 (FAT) != 4 (HD)
Warning: number of sectors per track mismatches 32 (FAT) != 16 (HD)
  MS Data                     8192     499711     491520 [boot]
  Linux filesys. data      1000001   10498008    9498008 [ROOT_MNJRO]
  Linux filesys. data     13344766   43961341   30616576 [retropie]
  Linux filesys. data     21733374   52349949   30616576 [retropie]



The harddisk (15 GB / 14 GiB) seems too small! (< 26 GB / 24 GiB)
Check the harddisk size: HD jumper settings, BIOS detection...

The following partitions can't be recovered:
     Partition               Start        End    Size in sectors
>  Linux filesys. data     13344766   43961341   30616576 [retropie]
   Linux filesys. data     21733374   52349949   30616576 [retropie]

vs Disk /dev/mmcblk0 - 15 GB / 14 GiB - CHS 486192 4 16
     Partition               Start        End    Size in sectors
>D MS Data                    62500    1000000     937501 [BOOT_MNJRO]
 D Linux filesys. data        94206   31116285   31022080


````










# kernel


https://gitlab.com/openbsd98324/linux-kernel-collection/-/blob/main/1651568499-1-Linux-kernel-raspbian-stretch-4.9.41-v7%2B-classic-2017-kernel-modules-sd-v1-piw-bogo-38-40-armv7-proc-rev-4-v7l-1.tar.gz


# qemu


```
qemu-system-arm -kernel qemu-kernel-4.9.41-cpu arm1176 -m 256 -M versatilepb -serial stdio -append "root=/dev/sda2 rootfstype=ext4 rw" -hda formatted_2017-11-29-raspbian-stretch-lite.img

```
